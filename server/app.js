const express = require('express')
const app = express()
const mongoose = require('mongoose')
const PORT = 5000
const {MONGOURI} = require('./keys')

app.use(express.json())

// Connect to database
require('./models/user')
mongoose.connect(MONGOURI)
mongoose.connection.on('connected', () => {
    console.log('connected to database ...')
})

// Routes
app.use(require('./routes/auth'))

app.listen(PORT, () => {
    console.log("server running ...")
})