const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const User = mongoose.model("User")
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../keys')
const requireAuth = require('../middleware/requireAuth')

router.post('/register', (req,res) => {
    const {firstname,lastname,username,email,password} = req.body
    if(!email || !password || !username  || !firstname){
        return res.status(422).json({error:"Fill the required fields"})
    }
    User.findOne({email})
    .then(savedUser => {
        if(savedUser){
            return res.status(422).json({error:"Account already exists with this email"})
        }
        User.findOne({username})
        .then(savedUsername => {
            if(savedUsername){
                return res.status(422).json({error:"Username unavailable"})
            }
            bcrypt.hash(password, 12)
            .then(password => {
                const user = new User({
                    firstname,
                    lastname,
                    username,
                    email,
                    password
                })
                user.save()
                .then(user => {
                    res.status(201).json("Account created.")
                }).catch(err => res.status(422).json({error:"Some error occured. Try Again."}))
            }).catch(err=>console.log(err))
        }).catch(err => console.log(err))
    }).catch(err => console.log(err))
})

router.post('/login', (req,res) => {
    const {email,password} = req.body
    if(!email || !password){
        res.status(422).json({error:"Fill required Fields"})
    }
    User.findOne({email})
    .then(savedUser => {
        if(!savedUser){
            return res.status(422).json({error:"Invalid email or password"})
        }
        bcrypt.compare(password,savedUser.password)
        .then(passwordMatch => {
            if(passwordMatch){
                const token = jwt.sign({_id:savedUser._id}, JWT_SECRET)
                res.status(200).json({token})
            }else{
                return res.status(422).json({error:"Invalid email or password"})
            }
        }).catch(err => console.log(err))
    }).catch(err => console.log(err))
})

router.get('/', requireAuth, (req,res) => {
    res.send("Authorised")
})

module.exports = router